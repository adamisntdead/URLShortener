var express = require('express');
var router = express.Router();

var validUrl = require('valid-url');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'URL Shortener Microservice'
    });
});

router.get('/new/:url(*)', function(req, res, next) {
    var params = req.params.url;

    var local = req.get('host') + "/";

    if (validUrl.isUri(params)) {
        // if URL is valid, do this
        var shortCode = new Buffer(params).toString('base64');
        res.json({
            original_url: params,
            short_url: local + shortCode
        });
    }
    else {
        // if URL is invalid, do this
        res.json({
            error: "Wrong url format, make sure you have a valid protocol and real site."
        });
    }
});

router.get('/:short', function(req, res) {
    res.redirect(new Buffer(req.params.short, 'base64').toString('ascii'));
});

module.exports = router;