# URL Shortener Micro-service (FreeCodeCamp)
This is the backend project for FreeCodeCamp - See [Here](https://www.freecodecamp.com/challenges/url-shortener-microservice)

## Usage
Checkout [here!] (http://shorter-urls.herokuapp.com/)

## Installation
1. `git clone https://github.com/adamisntdead/URLShortener.git`
2. `cd URLShortener`
3. `npm install`
4. `npm start`

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## User Storys
I can pass a URL as a parameter and I will receive a shortened URL in the JSON response.


If I pass an invalid URL that doesn't follow the valid http://www.example.com format, the JSON response will contain an error instead.


When I visit that shortened URL, it will redirect me to my original link.

## License
Released under GNU (General Public License)
Permission for individuals, Organizations and Companies to run, study, share (copy), and modify the software and its source.
Copyright Adam Kelly 2016-2017
